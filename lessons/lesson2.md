#Lesson 2: Creating Your First App#
Now that a project has been started and we have confirmed that it runs without errors, it's time to write guts of our website. Django has two concepts to help encapsulate work. One is a "project" and another is an "app".

A project is the website that is being created. This would be Openmeet in this case. A project is comprised of one or many "apps" and these apps may be reused in various projects. Apps should focus on a small set of features or one aspect of your overall project. It is not uncommon for a Django project to comprise of 8 or more apps. This is a good thing though because it keeps apps easy to maintain and reuseable. Here is what the [offcial Django documentation](https://docs.djangoproject.com/en/1.7/ref/applications/#projects-and-applications) has to say about projects and apps.


Let's create our first app. It will be in charge of all of the functionality behind a "Group" in Openmeet. A Group in Openmeet is something that has reaccuring meetings on a single subject (the Twin Cities Django Meetup would be an example of one of these). Users can subscribe to a group and a group can schedule meetings. Our Group app will be in charge of all group functionality and nothing else.

To create an app called "group" run the following:

	python manage.py startapp groups
	
This will create a new directory/python package with an app skeleton in it.


When we create an app, our Django project is not aware of it until we register it with our project. To do this, open up the settings.py file in the openmeet directory, locate the area with INSTALLED_APPS, and add 'groups' before the closing parenthesis. It should end up looking like the following:
```
INSTALLED_APPS = (
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'groups',
)
```


##Creating Your First Model
Next we are going to create a model to represent our groups. Models are a single definitive source of information about one piece of your application's data. You can read more about models in the [official Django documentation](https://docs.djangoproject.com/en/1.7/topics/db/models/). Open up the models.py file in the groups directory and enter the following:

```
from django.db import models
	
class Group(models.Model):
	title = models.CharField(max_length=150)
```

What this does is create a new model called "Group" that has a "title" that can be no longer than 150 characters.


Next we will need to create and run a database migration to apply the model to the database. Migrations are created by scanning all the models in all the registered apps in your project and creating a migration file to represent the changes in the modles. To scan your project for changes and write the migration files run the following:

	python manage.py makemigrations
	
To apply the migrations, run the following:

	python manange.py migrate


##The Django Admin
One of Django's greatest features is it's admin. The admin allows user to login and view and change model data. To learn more about the admin in detail, you can read the [official documentation](https://docs.djangoproject.com/en/1.7/ref/contrib/admin/). As of Django 1.6, the admin is available be default in new projects, but before we go to it, we need to create an admin user for it. While the admin does exist, it starts with no registered users. To create an admin user run the following:

	python manange.py syncdb

Follow the prompts and once you are done, start the development server by running `python manange.py runserver 0.0.0.0:8080` and visit [http://localhost:8080/admin](http://localhost:8080/admin) to get to the admin login page. Enter the credentials you entered after runing syncdb. Once you are logged in, you will see two models available for editing: Users and Groups. Django comes with a set of built in apps that are registered to new projects by default. The Users and Groups models come from the built in auth app. This is where the admin user that you just created lives. You won't see your Group model since it has not been registerd with the admin yet. Next we'll register our new Group model with admin so new Groups can be be created and edited.

##Registering Your Model To The Django Admin
Django does not make any model that you create available through the admin by default; they have to be registered with the admin first. This is why we did not see our Group model in the admin. To register the Group model to the admin, open up the admin.py file in the groups directory and add the following:
```
from django.contrib import admin
from groups.models import Group

admin.site.register(Group)                                   
```

Make sure the development server is running, then visit [http://localhost:8080/admin](http://localhost:8080/admin) and you should see your Group model now available. Feel free to play around with the admin. Try creating, editing and deleting some Groups.

This concludes lesson 2.


#Lesson Outline
- Create first app (Group).
- Understand the Model.
- Use the admin panel.
- We view model in admin panel.