# Lesson 1 #

# Part A: Setting Up Your Environment #

- Link to Windows Setup Instructions
- Link to Linux Setup Instructions
- Link to Mac Setup Instructions

    git clone repo
    cd cloned_repo

After you've setup your develpment environment as per the instructions above, you will need to run the following command:

    vagrant up

If you haven't already downloaded the vagrant box file, the file will begin to download.  This may take a few minutes depending on your internet connection.  After vagrant has finished initiating you will need to run the following command:

    vagrant ssh

    cd /vagrant

    django-admin.py startproject openmeet

Now that you've created your project let's start the development server.

    cd openmeet
    python manage.py runserver 0.0.0.0:8000

Now you can open your [browser](http://localhost:8080/).

This concludes lesson 1.
